import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StockReportRoutingModule } from './stock-report-routing.module';
import { StockReportComponent } from './stock-report.component';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import {MatTreeModule} from '@angular/material/tree';
import { MatIconModule } from '@angular/material';
@NgModule({
  declarations: [StockReportComponent],
  imports: [
    CommonModule,
    StockReportRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    MatTreeModule,
    MatIconModule
  ]
})
export class StockReportModule { }
