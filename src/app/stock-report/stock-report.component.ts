import { Component, OnInit } from '@angular/core';
//tree view
import { NestedTreeControl} from '@angular/cdk/tree';
import { MatTreeNestedDataSource} from '@angular/material/tree';
import { HttpService } from '../services/http.service';
import { FormBuilder } from '@angular/forms';
import { MessageService } from '../services/message.service';

interface FoodNode {
  id:number,
  organizationName: string;
  organizations?: FoodNode[];
}
//tree view end
@Component({
  selector: 'app-stock-report',
  templateUrl: './stock-report.component.html',
  styleUrls: ['./stock-report.component.css']
})
export class StockReportComponent implements OnInit {
  title ="Stock Report";
  orgGetUrl = "organization/ministry";
  reportUrl = "stock/report";
  addForm:any;
  reportData:any = [];
  TREE_DATA: FoodNode[] =[];//tree data
  validationMsg:string; 
  //tree view
  treeControl = new NestedTreeControl<FoodNode>(node => node.organizations);
  dataSource = new MatTreeNestedDataSource<FoodNode>();
  //tree view end  
  constructor(
    private httpService : HttpService,
    private fb: FormBuilder,
    private msgService : MessageService
  ) { }

  hasChild = (_: number, node: FoodNode) => !!node.organizations && node.organizations.length > 0;//tree
  ngOnInit() {
    //get org tree data from api
    this.httpService.oldGetWithToken(this.orgGetUrl).subscribe(
      res=> {
        this.TREE_DATA = res;
        this.dataSource.data = this.TREE_DATA;
      }
    )
    //get org tree data from api end

    //add form
    this.addForm = this.fb.group({
      organizationId: [''],
      fromDate: [''],
      toDate: ['']
    })
  }

  onSubmit()
  {
    this.validationMsg='';
    if(this.addForm.get('fromDate').value > this.addForm.get('toDate').value)
    {
      this.validationMsg = "From date must be less than to date";
      return;
    }
    this.httpService.postWithToken(this.reportUrl,this.addForm.value).subscribe(
      res=> {
        if(res.length)  {
          this.reportData = res;
          
        }
        else{
          this.reportData=[];
          this.msgService.add('No record found');
        }
       
      }
    )
  }
}
