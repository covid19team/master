import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ItemSubgroupComponent } from './item-subgroup.component';

const routes: Routes = [
  {
    path:'',
    component:ItemSubgroupComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ItemSubgroupRoutingModule { }
