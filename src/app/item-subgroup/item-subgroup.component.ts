import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MessageService } from '../services/message.service';
import { HttpService } from '../services/http.service';

@Component({
  selector: 'app-item-subgroup',
  templateUrl: './item-subgroup.component.html',
  styleUrls: ['./item-subgroup.component.css']
})
export class ItemSubgroupComponent implements OnInit {
  title ="Item sub Group";
  //api url
  getGroupAllUrl = "itemgroup/get/all";
  getAllUrl = "itemsubgroup/get/all";
  saveUrl = "itemsubgroup/save";
  editUrl = 'itemsubgroup/edit';

  addForm:any;
  updateForm:any;
  groupItems:any=[];
  subGroupItems:any=[];
  selectedItem:any;
  changedItem:any;

  constructor(
    private fb: FormBuilder,
    private msgService: MessageService,
    private httpService: HttpService
  ) { }

  ngOnInit() {
    this.getSubGroupItems();
    this.getGroupItems();
    //add form
    this.addForm = this.fb.group({
      itemGroupId: [''],
      subGroupName:['']
    })
    //update form
    this.updateForm = this.fb.group({
      id: [''],
      itemGroupId: [''],
      subGroupName:['']
    })

  }

  get itemGroupId(){return this.addForm.get('itemGroupId')}
  get itemsubGroupNameGroupId(){return this.addForm.get('subGroupName')}

  getGroupItems()
  {
    this.httpService.getWithToken(this.getGroupAllUrl).subscribe((res)=> {
     
      this.groupItems =res;
     
    });    
  }
  getSubGroupItems()
  {
    this.httpService.getWithToken(this.getAllUrl).subscribe((res)=> {
     
      this.subGroupItems =res;
      this.msgService.add("Item sub group fetched!");
    });
  }
  
  onSubmit()
  {
 
    this.httpService.postWithToken(this.saveUrl,this.addForm.value).subscribe(
      (res)=> {
        this.msgService.add(res.messgae);
        if(res.status=="ok"){
         let item = {
           id: res.id,
           itemGroupId: this.addForm.get('itemGroupId').value,
           itemGroupName: this.changedItem,
           subGroupName: this.addForm.get('subGroupName').value,
         }
         this.subGroupItems.unshift(item);
         this.addForm.reset();
        } 
      }
    )
  }

  edit(item: any):void
  {
    this.selectedItem = item;
   
    this.updateForm.get('id').value = this.selectedItem.id;
    this.updateForm.get('itemGroupId').value = this.selectedItem.itemGroupId;
    this.updateForm.get('subGroupName').value = this.selectedItem.subGroupName;
  }
  update()
  {
    
    let update = this.updateForm.value;
    let groupId = this.updateForm.get('itemGroupId').value;
    let groupName;
    for(let item of this.groupItems){
      if(item.id == groupId){groupName = item.name}
    }

    this.httpService.postWithToken(this.editUrl,update).subscribe((res)=>{
      this.msgService.add(res.messgae);
      if(res.status=="ok"){
        
        this.subGroupItems.filter((i)=>{
          if(i.id == update.id)
          {
            i.subGroupName = update.subGroupName;
            //i.itemGroupId = update.itemGroupId;
            i.itemGroupName = groupName;
          }
        })
      }
    })

  }
  onChange($event)
  {
     this.changedItem=  $event.target.options[$event.target.options.selectedIndex].text;
  }
}
