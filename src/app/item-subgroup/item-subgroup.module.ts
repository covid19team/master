import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ItemSubgroupRoutingModule } from './item-subgroup-routing.module';
import { ItemSubgroupComponent } from './item-subgroup.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [ItemSubgroupComponent],
  imports: [
    CommonModule,
    ItemSubgroupRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class ItemSubgroupModule { }
