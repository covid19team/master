import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemSubgroupComponent } from './item-subgroup.component';

describe('ItemSubgroupComponent', () => {
  let component: ItemSubgroupComponent;
  let fixture: ComponentFixture<ItemSubgroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemSubgroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemSubgroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
