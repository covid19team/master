import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DamageRoutingModule } from './damage-routing.module';
import { DamageComponent } from './damage.component';
import { AddDamageComponent } from './add-damage/add-damage.component';
import { DamageMainComponent } from './damage-main/damage-main.component';

@NgModule({
  declarations: [DamageComponent, AddDamageComponent, DamageMainComponent],
  imports: [
    CommonModule,
    DamageRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class DamageModule { }
