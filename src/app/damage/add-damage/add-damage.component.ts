import { Component, OnInit } from '@angular/core';
import { MessageService } from 'src/app/services/message.service';
import { HttpService } from 'src/app/services/http.service';
import { FormBuilder, FormArray } from '@angular/forms';

@Component({
  selector: 'app-add-damage',
  templateUrl: './add-damage.component.html',
  styleUrls: ['./add-damage.component.css']
})
export class AddDamageComponent implements OnInit {

  damageAllUrl = "benefititem/get/all";
  groupItemUrl = "itemgroup/get/all";
  subgroupBygroupIdUrl = "itemgroup/get/child";
  itemBySubgroupIdUrl = "itemsubgroup/get/child";
  itemDetailUrl = "item/get/child";  
  damageSaveUrl = "damage/save";
  getStockUrl = "stock/getStock";

  groupItem:any = [];
  addForm: any;
  stock:number=0;

  constructor(
    private msgService: MessageService,
    private httpService: HttpService,
    private fb: FormBuilder
  ) { }
  

  ngOnInit() {
    this.getGroupItems();

    //add form 
    this.addForm = this.fb.group({
      reason:[''],
      invItem: this.fb.array([
        this.initInvItem()
      ])
    })
    //add form end
  }

  get invItem(){return this.addForm.get('invItem') as FormArray}

  initInvItem()
  {
    return this.fb.group({
     
      itemId:[],
    
      invCountryOfOriginId:[],
      invBrandId:[],
      quantity:[],
      remarks:[]
    });
  }

  addInvItem()
  {
    this.invItem.push(this.initInvItem());
  }

  removeInvItem(i:number)
  {
    this.invItem.removeAt(i);
  }

  getGroupItems()
  {
    this.httpService.getWithToken(this.groupItemUrl).subscribe(res=>{
      this.groupItem = res
    })
  }
  //take 2 param index and group id
  getSubgroupItems(index:number,groupId:number)
  {
    let div = 'subgroupId'+index;
    let markup = '<option value="">Select</option>';

    if(groupId){
      this.httpService.getWithToken(this.subgroupBygroupIdUrl+'/'+groupId).subscribe(
        res=> {
          if(res.length)
          {
            for(let r of res){
              markup += `<option value='${r.id}'>${r.name}</option>`;
            }
          }
          else
          {
            markup += '<option value="">No record found</option>'
          }
  
          document.querySelector("#"+div).innerHTML = markup;
        },
      )      
    }
  }

  getItems(index:number,subgroupId:number)
  {
  //take 2 param index and event

    let div = 'itemId'+index;
    let markup = '<option value="">Select</option>';

    if(subgroupId)
    {
      this.httpService.getWithToken(this.itemBySubgroupIdUrl+'/'+subgroupId).subscribe(
        res=> {
          
          if(res.length)
          {
            for(let r of res){
              markup += `<option value='${r.id}' id="${r.uom}">${r.name}</option>`;
            }
          }
          else
          {
            markup += '<option value="">No record found</option>'
          }
  
          document.querySelector("#"+div).innerHTML = markup;
        },
      )      
    }
 
  }

  setItemFeature(i:number,uom:string,itemId:number){
    
    let uomDiv = '#uomId'+i;
    let corDiv = '#cor'+i;
    let brandDiv = '#brand'+i;
    let corMarkup = `<option value=''>Select</option>`;
    let brandMarkup =  `<option value=''>Select</option>`;
    document.querySelector(uomDiv).innerHTML += `<option value="${uom}" selected>${uom}</option>`;
    //get item detail
    if(itemId){
      this.httpService.getWithToken(this.itemDetailUrl+'/'+itemId).subscribe(
        res=> {
          
          for(let cof of res.countryOrigins){
            corMarkup += `<option value="${cof.id}">${cof.name}</option>`;
          }
          for(let brand of res.brands){
            brandMarkup += `<option value="${brand.id}">${brand.name}</option>`;
          }
          document.querySelector(corDiv).innerHTML = corMarkup;
          document.querySelector(brandDiv).innerHTML = brandMarkup;
        }
      )
    }

  }

  onSubmit()
  {
    this.httpService.postWithToken(this.damageSaveUrl,this.addForm.value).subscribe(
      res => {
        this.msgService.add(res.messgae);
        if(res.status=="ok"){this.addForm.reset()}
      }
    )
  }

  getStock(itemId,originId,brandId,index){
    if(itemId=='' || originId=='' || brandId==''){
      alert('Select item, country of origin & brand to check stock');
      return;
    }
    let post = {
      itemId: itemId,
      invCountryOfOriginId: originId,
      invBrandId: brandId      
    }
    this.httpService.postWithToken(this.getStockUrl,post).subscribe(
      res=> {
        var inputElement = <HTMLInputElement>document.getElementById('stock'+index);
        inputElement.value = res.quantity;
      }
    )
  }
}
