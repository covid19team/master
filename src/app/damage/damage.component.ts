import { Component, OnInit } from '@angular/core';
import { MessageService } from '../services/message.service';
import { HttpService } from '../services/http.service';
import { FormBuilder,FormArray } from '@angular/forms';


@Component({
  selector: 'app-damage',
  templateUrl: './damage.component.html',
  styleUrls: ['./damage.component.css']
})
export class DamageComponent implements OnInit {
  getAllUrl = "damage/allList";
  title ="Damage";

  damages:any = [];
  pageNumber = 0;
  constructor(
    private msgService: MessageService,
    private httpService: HttpService,
    private fb: FormBuilder,
  ) { }

  ngOnInit() {
    //get damages
    this.getDamage();
  }

  getDamage(page= 0)
  {
    this.pageNumber = page;
    let post = {pageNumber:page, pageSize:10};
    this.httpService.postWithToken(this.getAllUrl,post).subscribe(
      res=> {
        if(res.length)
        {
          this.msgService.add('Damage fetched successfully!');
        }
        else
        {
          this.msgService.add('No record found!');
        }
        this.damages = res;
      }
    )
  }
}
