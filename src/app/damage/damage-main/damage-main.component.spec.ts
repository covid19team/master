import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DamageMainComponent } from './damage-main.component';

describe('DamageMainComponent', () => {
  let component: DamageMainComponent;
  let fixture: ComponentFixture<DamageMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DamageMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DamageMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
