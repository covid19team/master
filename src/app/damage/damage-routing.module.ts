import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DamageComponent } from './damage.component';
import {AddDamageComponent} from './add-damage/add-damage.component';
import {DamageMainComponent} from './damage-main/damage-main.component';

const routes: Routes = [
  {
    path:'',
    component: DamageMainComponent,
    children:[
      {
        path:'',
        component:DamageComponent
      },
      {
        path:'add',
        component:AddDamageComponent
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DamageRoutingModule { }
