import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators} from '@angular/forms';
import { HttpService } from '../services/http.service';
import { AuthService } from '../services/auth.service';
//loading bar indicator 
import {
  Event,
  NavigationCancel,
  NavigationEnd,
  NavigationError,
  NavigationStart,
  Router
} from '@angular/router';
import { MessageService } from '../services/message.service';

//loading bar indicator end
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm;
  userInfoUrl = "user";
  loginUrl = "authenticate"; 

  userData:any;
  loading = false;
  constructor(
    private fb: FormBuilder,
    private httpService: HttpService,
    public auth: AuthService,
    private router: Router,
    private msgService: MessageService
  ) { 
//loading
    this.router.events.subscribe((event: Event) => {
      switch (true) {
        case event instanceof NavigationStart: {
          this.loading = true;
          break;
        }

        case event instanceof NavigationEnd:
        case event instanceof NavigationCancel:
        case event instanceof NavigationError: {
          this.loading = false;
          break;
        }
        default: {
          break;
        }
      }
    });
  }
//loading end

  ngOnInit() {
    //login form validation
    this.loginForm = this.fb.group({
      username : ['',[Validators.required]],
      password: ['',[Validators.required]]
    })   

  }

  get username(){return this.loginForm.get('username')}
  get password(){return this.loginForm.get('password')}

  onSubmit(){
    let username = this.loginForm.get('username').value;
    
    this.httpService.post(this.loginUrl,this.loginForm.value).subscribe(
      (res)=> {
            //authenticate user
            //alert('login done')
            let authInfo = {username:this.loginForm.get('username').value,token:res.id_token};
            this.httpService.userInfo(this.userInfoUrl,res.id_token).subscribe(
              res=>{
                //alert(JSON.stringify(res))
                //setting org
                localStorage.setItem('organization',res.organization);
                localStorage.setItem('organizationName',res.organizationName);                
                this.auth.authenticate(authInfo);
                this.msgService.add("Login success!");                    

                
              }
            )        
     
      },
      //res=> alert(JSON.stringify(res)),
     
    )
  }

  userInfo()
  {

  }
}
