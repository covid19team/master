
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent} from "./login.component";
import { LoginRoutingModule } from './login-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { MatProgressBarModule } from '@angular/material/progress-bar';
@NgModule({
  declarations: [
    LoginComponent
  ],
  imports: [

    CommonModule,
    LoginRoutingModule,
    ReactiveFormsModule,
    MatProgressBarModule
  ]
})
export class LoginModule { }
