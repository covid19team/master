import { Injectable, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService implements OnInit{
  userToken:any = localStorage.getItem('token');
  userName:any = localStorage.getItem('username');
 
  redirectUrl: string;
  constructor(
    private router: Router
  ) { }
  ngOnInit()
  {
    
  }
  authenticate(userInfo:any)
  { 
  /* get user info*/
       
  /* get user info*/     
    localStorage.setItem('username',userInfo.username);
    localStorage.setItem('token',userInfo.token);
    this.userToken = localStorage.getItem('token');
    this.userName = localStorage.getItem('username');
    
    this.router.navigate(['/']);
  }

  logout()
  {
    
    localStorage.clear();
    this.userName = '';
    this.userToken = '';
    
    this.router.navigate(['/login']);
    //window.location.href="http://localhost:4200";
  }
}
