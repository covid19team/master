import { Injectable, OnInit } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class HttpService implements OnInit{
  token:any = localStorage.getItem('token');
  
  httpOptions = {
    headers: new HttpHeaders({ 
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.token
      //'Authorization': 'Bearer ' + this.authService.userToken
    })
  }; 

  URL = "http://35.226.65.139:8080/inventory/api/";
 // URL = "http://localhost:8889/api/";
  OLDURL = "http://35.226.65.139:8080/covid25/api/";
  
  constructor(
    private http: HttpClient, 
    private authService: AuthService
  ) { }

  ngOnInit() {
    //this.token= localStorage.getItem('token');
          
  }
  getWithToken (url:any): Observable<any>
  {
    
    return this.http.get<any>(this.URL+url,this.httpOptions);
    //return this.data;
  }

  postWithToken(url:any,data:any): Observable<any>
  {
    return this.http.post(this.URL+url,data,this.httpOptions);
  }
  
  deleteWithToken(url:any): Observable<any>
  {
    return this.http.delete(this.URL+url, this.httpOptions);
  }

  get (url:any): Observable<any>
  {
 
    return this.http.get<any>(this.URL+url);
    //return this.data;
  }  
  post(url:any,data:any): Observable<any>
  {
    return this.http.post(this.URL+url,data);
  }

//old api methods  
userInfo (url:any,token:any): Observable<any>
{
  let httpOptions = {
    headers: new HttpHeaders({ 
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + token
    })
  };  
  return this.http.get<any>(this.OLDURL+url,httpOptions);
  //return this.data;
}
oldGetWithToken (url:any): Observable<any>
{
  
  return this.http.get<any>(this.OLDURL+url,this.httpOptions);
  //return this.data;
}

oldPostWithToken(url:any,data:any): Observable<any>
{
  return this.http.post(this.OLDURL+url,data,this.httpOptions);
}
//old api methods end  
authenticate(url:any,data:any): Observable<any>
{
  return this.http.post(this.URL+url,data);
}
}
