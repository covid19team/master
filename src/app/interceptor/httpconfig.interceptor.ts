import { Injectable } from '@angular/core';
import {
    HttpInterceptor,
    HttpRequest,
    HttpResponse,
    HttpHandler,
    HttpEvent,
    HttpErrorResponse
} from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { map, catchError, finalize } from 'rxjs/operators';
import { ErrordialogService } from '../services/errordialog.service';
import { LoaderService } from '../services/loader.service';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Injectable()
export class HttpConfigInterceptor implements HttpInterceptor {
    constructor(
        public errorDialogService: ErrordialogService,
        public loaderService: LoaderService,
        private router: Router,
        private authService: AuthService
    ){}
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        this.loaderService.show();
        return next.handle(request).pipe(
                map((event: HttpEvent<any>) => {
                    if (event instanceof HttpResponse) {
                        //console.log('event--->>>', event);
                    }
                    return event;
                }),
                catchError((error: HttpErrorResponse) => {
                   
                    let data = {};
                    data = {
                        reason: error.error.message,
                        status: error.status
                    };
                    
                    this.errorDialogService.openDialog(data);
                    
                    return throwError(error);
                }),
                finalize(()=>{this.loaderService.hide();})
            );
    }
}