import { Component, OnInit } from '@angular/core';
import { MessageService } from 'src/app/services/message.service';
import { HttpService } from 'src/app/services/http.service';
import { FormBuilder, FormArray, Validators } from '@angular/forms';

//tree view
import {NestedTreeControl} from '@angular/cdk/tree';
import {MatTreeNestedDataSource} from '@angular/material/tree';
import { repeat } from 'rxjs/operators';
interface FoodNode {
  id:number,
  organizationName: string;
  organizations?: FoodNode[];
}
//tree view end

@Component({
  selector: 'app-add-demand',
  templateUrl: './add-demand.component.html',
  styleUrls: ['./add-demand.component.css']
})
export class AddDemandComponent implements OnInit {
  
  orgGetUrl = "organization/ministry";
  benefitAllUrl = "benefitItem/all";
  benefitDetailUrl = "report/numberOfBenefit";
  groupItemUrl = "itemgroup/get/all";
  subgroupBygroupIdUrl = "itemgroup/get/child";
  itemBySubgroupIdUrl = "itemsubgroup/get/child";
  itemDetailUrl = "item/get/child";
  getStockUrl = "stock/getStock";
  demandSaveUrl = "invDemand/save";

  TREE_DATA: FoodNode[] =[];//tree data
  totalNum= {sum:0,list:[]};
  benefitItems: any = [];
  addForm: any;
  groupItem:any = [];
  subgroupItem:any= [];
//tree view
treeControl = new NestedTreeControl<FoodNode>(node => node.organizations);
dataSource = new MatTreeNestedDataSource<FoodNode>();
//tree view end
  constructor(
   
    private msgService: MessageService,
    private httpService: HttpService,
    private fb: FormBuilder,
    
  ) { }

  hasChild = (_: number, node: FoodNode) => !!node.organizations && node.organizations.length > 0;//tree
  ngOnInit() {
    //get org tree data from api
    this.httpService.oldGetWithToken(this.orgGetUrl).subscribe(
      res=> {
        this.TREE_DATA = res;
        this.dataSource.data = this.TREE_DATA;
      }
    )
    //get org tree data from api end
    

    this.getBenefitItems();
    this.getGroupItems();

    //add form 
    this.addForm = this.fb.group({
      benefitItemId:[''],
      
      requestedTo:[''],
      invItem: this.fb.array([
        this.initInvItem()
      ])
    })
    //add form end
  }

  get invItem(){return this.addForm.get('invItem') as FormArray}

  initInvItem()
  {
    return this.fb.group({
     
      itemId:[],
    
      invCountryOfOriginId:[],
      invBrandId:[],
      quantity:[],
      remarks:[]
    });
  }

  addInvItem()
  {
    this.invItem.push(this.initInvItem());
  }

  removeInvItem(i:number)
  {
    this.invItem.removeAt(i);
  }

  getBenefitItems()
  {
  
    this.httpService.oldGetWithToken(this.benefitAllUrl).subscribe(res=>{
      
      this.benefitItems = res
    })
  }
  getGroupItems()
  {
    this.httpService.getWithToken(this.groupItemUrl).subscribe(res=>{
      this.groupItem = res
    })
  }
  //take 2 param index and group id
  getSubgroupItems(index:number,groupId:number)
  {
    let div = 'subgroupId'+index;
    let markup = '<option value="">Select</option>';

    if(groupId){
      this.httpService.getWithToken(this.subgroupBygroupIdUrl+'/'+groupId).subscribe(
        res=> {
          if(res.length)
          {
            for(let r of res){
              markup += `<option value='${r.id}'>${r.name}</option>`;
            }
          }
          else
          {
            markup += '<option value="">No record found</option>'
          }
  
          document.querySelector("#"+div).innerHTML = markup;
        },
      )      
    }
  }

  getItems(index:number,subgroupId:number)
  {
  //take 2 param index and event

    let div = 'itemId'+index;
    let markup = '<option value="">Select</option>';

    if(subgroupId)
    {
      this.httpService.getWithToken(this.itemBySubgroupIdUrl+'/'+subgroupId).subscribe(
        res=> {
          
          if(res.length)
          {
            for(let r of res){
              markup += `<option value='${r.id}' id="${r.uom}">${r.name}</option>`;
            }
          }
          else
          {
            markup += '<option value="">No record found</option>'
          }
  
          document.querySelector("#"+div).innerHTML = markup;
        },
      )      
    }
 
  }

  setItemFeature(i:number,uom:string,itemId:number){
    
    let uomDiv = '#uomId'+i;
    let corDiv = '#cor'+i;
    let brandDiv = '#brand'+i;
    let corMarkup = `<option value=''>Select</option>`;
    let brandMarkup =  `<option value=''>Select</option>`;
    //document.querySelector(uomDiv).value += `<option value="${uom}" selected>${uom}</option>`;
    var inputElement = <HTMLInputElement>document.querySelector(uomDiv);
    inputElement.value = uom;
    //get item detail
    if(itemId){
      this.httpService.getWithToken(this.itemDetailUrl+'/'+itemId).subscribe(
        res=> {
          
          for(let cof of res.countryOrigins){
            corMarkup += `<option value="${cof.id}">${cof.name}</option>`;
          }
          for(let brand of res.brands){
            brandMarkup += `<option value="${brand.id}">${brand.name}</option>`;
          }
          document.querySelector(corDiv).innerHTML = corMarkup;
          document.querySelector(brandDiv).innerHTML = brandMarkup;
        }
      )
    }

  }
  checkTotalBenefit(benItem:number)
  {
    
    if(benItem)
    {
      this.httpService.getWithToken(this.benefitDetailUrl+'/'+benItem).subscribe(
        res=>{
         
          this.totalNum.sum = res.sum;
          this.totalNum.list = res.list;
        }
      )
    }
  }
  checkStock(index,itemId,cor,brand)
  {

    let div = "#stock"+index;
         
    if(itemId =='' || cor =='' || brand ==''){
      alert('Select item id , country of origin & brand');
      return;
    }
 
    let obj = {itemId: itemId,invCountryOfOriginId:cor,invBrandId:brand};
    this.httpService.postWithToken(this.getStockUrl,obj).subscribe(
      res=> {
        if(res.quantity)
        {

          document.querySelector(div).innerHTML= res.quantity;
        }
        else{this.msgService.add(res.messgae)}
      }
    )
  }
  onSubmit(){
    let items = this.invItem.value;
    let check = this.checkDuplicateInObject(['itemId','invCountryOfOriginId','invBrandId'],items);
    for(let i=0; i<items.length;i++)
    {
      let j = i+1;
      if(items.length >j)
      {
        for(j; j<items.length;j++)
        {
          
          if(items[i].itemId==items[j].itemId && items[i].invCountryOfOriginId==items[j].invCountryOfOriginId &&items[i].invBrandId==items[j].invBrandId)
          {
            alert(`Record ${i+1} & ${j+1} are duplicate entry!`);
            return ;
          }

        }
      }
    }

      this.httpService.postWithToken(this.demandSaveUrl,this.addForm.value).subscribe(
        res=> {
          this.msgService.add(res.messgae);
          if(res.status=='ok'){
            this.addForm.reset();
            this.totalNum.sum=0;
          }
        }
      )

  }

  checkDuplicateInObject(propertyName, inputArray) {
    var seenDuplicate = false,
        testObject = {};
  
    inputArray.map(function(item) {
      var itemPropertyName = item[propertyName];
      if (propertyName[0] in testObject && propertyName[1] in testObject && propertyName[2] in testObject) {
        testObject[itemPropertyName].duplicate = true;
        item.duplicate = true;
        seenDuplicate = true;
      }
      else {
        testObject[itemPropertyName] = item;
        delete item.duplicate;
      }
    });
  
    return seenDuplicate;
  }  
}
