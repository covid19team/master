import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpService } from 'src/app/services/http.service';
import { MessageService } from 'src/app/services/message.service';

@Component({
  selector: 'app-demand-detail',
  templateUrl: './demand-detail.component.html',
  styleUrls: ['./demand-detail.component.css']
})
export class DemandDetailComponent implements OnInit {

  demandDetailUrl = "invDemand/get";
  title ="Demand Detail";
  detail:any=[];
  constructor(
    private route : ActivatedRoute,
    private http : HttpService,
    private msg : MessageService
  ) { }

  ngOnInit() {
    const demandId = this.route.snapshot.paramMap.get('id');
    this.demandDetail(demandId);
  }

  demandDetail(demandId:any)
  {
    this.http.getWithToken(this.demandDetailUrl+'/'+demandId).subscribe(res=>{
      
      this.detail = res;
      this.msg.add('Demand detail fetched!');
    })
  }

}
