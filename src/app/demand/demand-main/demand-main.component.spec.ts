import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DemandMainComponent } from './demand-main.component';

describe('DemandMainComponent', () => {
  let component: DemandMainComponent;
  let fixture: ComponentFixture<DemandMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DemandMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DemandMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
