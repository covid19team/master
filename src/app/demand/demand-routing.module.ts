import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DemandComponent } from './demand.component';
import { AddDemandComponent } from './add-demand/add-demand.component';
import { DemandMainComponent } from './demand-main/demand-main.component';
import { DemandEditComponent } from './demand-edit/demand-edit.component';
import { DemandDetailComponent } from './demand-detail/demand-detail.component';


const routes: Routes = [
  {
    path:'',
    component: DemandMainComponent,
    children: [
      {
        path:'',
        component: DemandComponent
      },
      {
        path:'add',
        component: AddDemandComponent
      },
      {
        path:'detail/:id',
        component: DemandDetailComponent
      }      
    ]
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DemandRoutingModule { }
