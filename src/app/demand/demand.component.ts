import { Component, OnInit } from '@angular/core';
import { MessageService } from '../services/message.service';
import { HttpService } from '../services/http.service';


@Component({
  selector: 'app-demand',
  templateUrl: './demand.component.html',
  styleUrls: ['./demand.component.css']
})
export class DemandComponent implements OnInit {
  title:string ="Demand";
  //getAllUrl = "invDemand/get/all";
  getAllUrl = "invDemand/get/getAllRequestedBy";
  demandDeleteUrl = "invDemand/delete";
  demands:any = [];

  constructor(
    private msgService: MessageService,
    private httpService: HttpService,
  
  ) { }

  ngOnInit() {
    //get demand list
    this.httpService.getWithToken(this.getAllUrl).subscribe(
      res=> {
        this.msgService.add('Demand fetched successfully');
        this.demands = res
      }
    )
  }

  delete(demand:any,id:number)
  {

    if(confirm('Do you want to delete?'))
    {
      this.httpService.deleteWithToken(this.demandDeleteUrl+'/'+id).subscribe(
        res=> {
          this.msgService.add(res.messgae);
          if(res.status =='ok'){
            this.demands = this.demands.filter(d => d !==demand);
          }
        }
      )
            
    }
  

  }
}

