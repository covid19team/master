import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DemandRoutingModule } from './demand-routing.module';
import { DemandComponent } from './demand.component';
import { AddDemandComponent } from './add-demand/add-demand.component';
import { DemandMainComponent } from './demand-main/demand-main.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatTreeModule} from '@angular/material/tree';
import {MatIconModule} from '@angular/material/icon';
import {MatDialogModule} from '@angular/material/dialog';
import { DemandEditComponent } from './demand-edit/demand-edit.component';
import { DemandDetailComponent } from './demand-detail/demand-detail.component';
@NgModule({
  declarations: [DemandComponent, AddDemandComponent, DemandMainComponent, DemandEditComponent, DemandDetailComponent],
  imports: [
    CommonModule,
    DemandRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MatTreeModule,
    MatIconModule,
    MatDialogModule
  ],
  
})
export class DemandModule { }
