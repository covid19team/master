
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormArray, Form } from '@angular/forms';
import { HttpService } from '../services/http.service';
import { MessageService } from '../services/message.service';

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.css']
})
export class ItemsComponent implements OnInit {
  title ="Items";
  //api url
  saveUrl = "item/save";
  getAllUrl = "item/get/all";
  editUrl = "item/edit";
  groupItemAllUrl = 'itemgroup/get/all';
  subGroupItemAllUrl = 'itemsubgroup/get/all';
  subGroupByGroupIdUrl = 'itemgroup/get/child';
  //api url end
  addForm:any;
  updateForm:any;
  items:any = [];
  groupItems:any = [];
  subgroupItems:any = [];
  subgroupItemsAll:any = [];
  selectedItem:any;
  subgroupMsg = "select group name first";

  constructor(
    private fb: FormBuilder,
    private httpService: HttpService,
    private msgService: MessageService
  ) { }

  ngOnInit() {
    this.httpService.getWithToken(this.getAllUrl).subscribe(
      res=> this.items = res
    )
    
    this.groupItems = this.httpService.getWithToken(this.groupItemAllUrl).subscribe(
      res=> {
        this.groupItems = res;
        this.msgService.add("Items fetched");
      });
 
    this.subgroupItemsAll = this.httpService.getWithToken(this.subGroupItemAllUrl).subscribe(
     
      res=> {
        this.subgroupItemsAll = res;
        
      }
      
    );
    

    //add form
    this.addForm = this.fb.group({
      id: [null],
      itemSubGroupId: [''],
      itemName: [''],
      itemSKU: [''],
      uom: [''],
      
      itemfeature: this.fb.array([
        this.inititemfeature()
      ])
    });
    console.log(this.addForm.value)
    //add form end

    //update form
    this.updateForm = this.fb.group({
      id: [''],
      itemSubGroupId: [''],
      itemName: [''],
      itemSKU: [''],
      uom: [''],
      itemfeature: this.fb.array([])
    });  
    //update form end
  }

  get itemfeature() {
    return this.addForm.get('itemfeature') as FormArray;
  }
  get updateItemFeature(){
    return this.updateForm.get('itemfeature') as FormArray;
  }

  inititemfeature()
  {
    return this.fb.group({
      type: [''],
      name: [''],
    })
  }
  additemfeature()
  {
    //this.itemfeature.push({type: this.fb.control(''), name:this.fb.control('')});
    this.itemfeature.push(this.inititemfeature());
  }

  removeitemfeature(i:number)
  {
    
    this.itemfeature.removeAt(i);
  }
  //for update form add more item
  additemfeatureU()
  {
    //this.itemfeature.push({type: this.fb.control(''), name:this.fb.control('')});
    this.updateItemFeature.push(this.inititemfeature());
  }
  removeitemfeatureU(i:number)
  {
    
    this.updateItemFeature.removeAt(i);
  }  
  getSubgroupByGroupId(event)
  {

    this.subgroupItems = [];
    let groupId = event.target.value;//itemGroupId
   
    for(let sgi of this.subgroupItemsAll)
    {
      if(sgi.itemGroupId == groupId)
      {
        this.subgroupItems.push(sgi)
      }
    }
    if(this.subgroupItems.length<1){
      this.subgroupMsg = "no record found";
    }
   
  }
  onSubmit()
  {
    let subGroupName;
    for( let sgn of this.subgroupItemsAll){
      if(sgn.id==this.addForm.get('itemSubGroupId').value){
        subGroupName = sgn.subGroupName;
      }
     }
    this.httpService.postWithToken(this.saveUrl,this.addForm.value).subscribe(
      res=> {
        this.msgService.add(res.messgae);
        if(res.status=='ok')
        {
          let items = {
            itemSubGroupName: subGroupName,
            itemName: this.addForm.get('itemName').value,
            itemSKU: this.addForm.get('itemSKU').value,
            uom: this.addForm.get('uom').value,
            itemfeature: this.addForm.get('itemfeature').value,
          }
          this.items.unshift(items)          
        }
      }
    )
  }

  edit(item:any)
  {
    
    this.selectedItem = item;
    while(this.updateForm.get('itemfeature').length !==0)
    {
      this.updateForm.get('itemfeature').removeAt(0);
    }
    this.updateForm.get('id').value = this.selectedItem.id;
    this.updateForm.get('itemSubGroupId').value = this.selectedItem.itemSubGroupId;
    this.updateForm.get('itemName').value = this.selectedItem.itemName;
    this.updateForm.get('itemSKU').value = this.selectedItem.itemSKU;
    this.updateForm.get('uom').value = this.selectedItem.uom;
    for(let si of this.selectedItem.itemfeature){
      this.updateForm.get('itemfeature').push(this.fb.group(si))
    }
    
    
  }

  update()
  {
    let update = this.updateForm.value;
  
    this.httpService.postWithToken(this.editUrl,update).subscribe(
      res=> {
        this.msgService.add(res.messgae)
        if(res.status =='ok'){
          //get sub group name
          let subgroupName;

            for(let sgn of this.subgroupItemsAll){
              if(update.itemSubGroupId == sgn.id){
                subgroupName= sgn.name;
              }
            }
          

          //update item
          this.items.filter(i=>{
            if(i.id == update.id)
            {
              i.itemSubGroupName = subgroupName,
              i.itemName = update.itemName,
              i.itemSKU = update.itemSKU,
              i.uom = update.uom,
              i.itemfeature = update.itemfeature          
            }
          })
        }
      }
    )
 

   
  }

}
