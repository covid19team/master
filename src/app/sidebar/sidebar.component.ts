import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  auth = false;
  orgId: any;
  orgName: any;  
  constructor(
    
  ) { }

  ngOnInit() {
    this.orgId= localStorage.getItem('organization');
    this.orgName= localStorage.getItem('organizationName');       
  }

}
