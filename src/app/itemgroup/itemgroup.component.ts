import { Component, OnInit } from '@angular/core';
import { Item } from '../interface/item';
import { HttpService } from '../services/http.service';
import { MessageService } from '../services/message.service';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-itemgroup',
  templateUrl: './itemgroup.component.html',
  styleUrls: ['./itemgroup.component.css']
})
export class ItemgroupComponent implements OnInit {
  getAllUrl = "itemgroup/get/all";
  saveUrl = "itemgroup/save";
  editUrl = 'itemgroup/edit';

  title ="Item Group";
  items:any=[];
  selectedItem: any;
  itemGroupForm:any;
  itemGroupUpdateForm:any;
  showModal = true;
  
  constructor(
    private fb: FormBuilder,
    private httpService: HttpService,
    private msgService: MessageService,
  ) 
  { 

  }

  ngOnInit() {

    this.itemGroupForm = this.fb.group({
      groupName: ['']
    });
    //update form
    
      this.itemGroupUpdateForm = this.fb.group({
        id: [''],
        groupName:['']
      });
 

    //update form end
    //on init get items
    this.getItems();
   
  }
  get groupName(){return this.itemGroupForm.get('groupName')}
  
  getItems()
  {
    this.httpService.getWithToken(this.getAllUrl).subscribe((res)=> {
     
      this.items =res;
      this.msgService.add("Item group fetched!");
    });
  }
  
  edit(item: Item):void
  {
    this.selectedItem = item;
    this.itemGroupUpdateForm.get('id').value = this.selectedItem.id;
    this.itemGroupUpdateForm.get('groupName').value = this.selectedItem.name;
  }

  onSubmit()
  {
    console.log(this.itemGroupForm.value);
    this.httpService.postWithToken(this.saveUrl,this.itemGroupForm.value).subscribe(
      (res)=> {
        this.msgService.add(res.messgae);
        if(res.status=="ok"){
         let item = {
           id: res.id,
           name: this.itemGroupForm.get('groupName').value
         }
         this.items.unshift(item);
         this.itemGroupForm.reset();
        } 
      }
    )
  }

  update()
  {
    
    let update = this.itemGroupUpdateForm.value;
    this.httpService.postWithToken(this.editUrl,update).subscribe((res)=>{
      this.msgService.add(res.messgae);
      if(res.status=="ok"){
        this.showModal = false;
        this.items.filter((i)=>{
          if(i.id == update.id)
          {
            i.name = update.groupName;
          }
        })
      }
    })

  }
}
