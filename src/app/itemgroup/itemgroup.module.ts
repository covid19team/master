import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ItemgroupRoutingModule } from './itemgroup-routing.module';
import { ItemgroupComponent } from './itemgroup.component';

@NgModule({
  declarations: [
    ItemgroupComponent
  ],
  imports: [
    CommonModule,
    ItemgroupRoutingModule
  ]
})
export class ItemgroupModule { }
