import { Component, OnInit , Inject} from '@angular/core';
import { MAT_DIALOG_DATA,MatDialogRef } from '@angular/material';
@Component({
  selector: 'app-errordialog',
  templateUrl: './errordialog.component.html',
  styleUrls: ['./errordialog.component.css']
})
export class ErrordialogComponent implements OnInit {

  title = 'Angular-Interceptor';
  token = localStorage.getItem('token');
  //constructor(@Inject(MAT_DIALOG_DATA) public data: string) {}
  constructor(
    public dialogRef: MatDialogRef<ErrordialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: string) {}
  ngOnInit() {
   
  }
  onNoClick(): void {
    this.dialogRef.close();
    
  }
}
