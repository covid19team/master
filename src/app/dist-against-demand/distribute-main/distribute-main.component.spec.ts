import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DistributeMainComponent } from './distribute-main.component';

describe('DistributeMainComponent', () => {
  let component: DistributeMainComponent;
  let fixture: ComponentFixture<DistributeMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DistributeMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DistributeMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
