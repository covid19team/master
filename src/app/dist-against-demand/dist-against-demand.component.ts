import { Component, OnInit } from '@angular/core';
import { MessageService } from '../services/message.service';
import { HttpService } from '../services/http.service';
import { FormBuilder,FormArray } from '@angular/forms';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-dist-against-demand',
  templateUrl: './dist-against-demand.component.html',
  styleUrls: ['./dist-against-demand.component.css']
})
export class DistAgainstDemandComponent implements OnInit {
  title ="Distribute";
  closeResult: string;
  distributeList: any =[];
  allDistributedUrl = "distribute/getAll";
  singleDistributeUrl = "distribute/get";

  benefitItemName: string;
  distributeNo: string;
  receivedName: string;
  distributeDate: any ='';
  status: string; 
  distributeItemList: any = [];

  constructor(
    private httpService : HttpService,
    private modalService: NgbModal
  ) { }

  ngOnInit() {
    this.getAllDistributedInfo();
  }

  getAllDistributedInfo () {

    this.httpService.getWithToken(this.allDistributedUrl).subscribe (res =>{
      this.distributeList = res;
    })
  }

  // getting Single distribute by Id
 
  getDistributeById (id:number) {

    this.httpService.getWithToken(this.singleDistributeUrl+"/"+id).subscribe(res =>{
      this.benefitItemName = res[0].benefitItemName;
      this.distributeNo = res[0].distributeNo;
      this.receivedName = res[0].receivedName;
      this.distributeDate = res[0].distributeDate;
      this.status = res[0].status;
      this.distributeItemList = res[0].invItemBackendValue;
      
    })
  }

  //Open View Modal Pop-up
open(content) {
  this.modalService.open(content,  {ariaLabelledBy: 'modal-basic-title',  size: 'lg', windowClass: 'custom-class'}).result.then((result) => {
    this.closeResult = `Closed with: ${result}`;
  }, (reason) => {
    this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
  });
}

private getDismissReason(reason: any): string {
  if (reason === ModalDismissReasons.ESC) {
    return 'by pressing ESC';
  } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
    return 'by clicking on a backdrop';
  } else {
    return  `with: ${reason}`;
  }
}
}
