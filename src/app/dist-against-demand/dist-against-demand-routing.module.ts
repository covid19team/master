import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DistAgainstDemandComponent } from './dist-against-demand.component';
import {AddDistributeComponent} from './add-distribute/add-distribute.component';
import {DistributeMainComponent} from './distribute-main/distribute-main.component';
const routes: Routes = [
  {
    path: '',
    component: DistributeMainComponent,
    children: [
      {
        path:'',
        component:DistAgainstDemandComponent
      },
      {
        path:'add',
        component:AddDistributeComponent
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DistAgainstDemandRoutingModule { }
