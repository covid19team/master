import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { DistAgainstDemandRoutingModule } from './dist-against-demand-routing.module';
import { DistAgainstDemandComponent } from './dist-against-demand.component';
import { AddDistributeComponent } from './add-distribute/add-distribute.component';
import { DistributeMainComponent } from './distribute-main/distribute-main.component';
import {MatTreeModule} from '@angular/material/tree';
import {MatIconModule} from '@angular/material/icon';
import {MatFormFieldModule,MatInputModule} from '@angular/material';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [DistAgainstDemandComponent, AddDistributeComponent, DistributeMainComponent],
  imports: [
    CommonModule,
    DistAgainstDemandRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MatTreeModule,
    MatIconModule,
    MatFormFieldModule,
    MatInputModule,
    NgbModule
  ]
})
export class DistAgainstDemandModule { }
