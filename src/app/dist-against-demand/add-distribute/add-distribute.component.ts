import { Component, OnInit } from '@angular/core';
import { MessageService } from 'src/app/services/message.service';
import { HttpService } from 'src/app/services/http.service';
import { FormBuilder, FormArray } from '@angular/forms';
//tree view
import { NestedTreeControl } from '@angular/cdk/tree';
import { MatTreeNestedDataSource } from '@angular/material/tree';

interface FoodNode {
  id: number,
  organizationName: string;
  organizations?: FoodNode[];
}
//tree view end

@Component({
  selector: 'app-add-distribute',
  templateUrl: './add-distribute.component.html',
  styleUrls: ['./add-distribute.component.css']
})
export class AddDistributeComponent implements OnInit {

  benefitAllUrl = "benefititem/get/all";
  orgGetUrl = "organization/ministry";
  receivedByBenefitItemId = "receive/getReceiveBybenefitItemId";
  getSingleReceiveById = "receive/get";
  distributSaveUrl = "distribute/save";

  TREE_DATA: FoodNode[] = [];//tree data
  benefitItems: any = [];
  addForm: any;
  receivedItemList: any = [];
  //tree view
  treeControl = new NestedTreeControl<FoodNode>(node => node.organizations);
  dataSource = new MatTreeNestedDataSource<FoodNode>();
  hasChild = (_: number, node: FoodNode) => !!node.organizations && node.organizations.length > 0;//tree
  //tree view end
  constructor(
    private msgService: MessageService,
    private httpService: HttpService,
    private fb: FormBuilder
  ) { }


  ngOnInit() {

    //get org tree data from api
    this.httpService.oldGetWithToken(this.orgGetUrl).subscribe(
      res => {
        this.TREE_DATA = res;
        this.dataSource.data = this.TREE_DATA;
      }
    )
    //get org tree data from api end
    this.getBenefitItems();

    //add form 
    this.addForm = this.fb.group({
      ownOrganization: [''],
      benefitItemId: [''],
      receivedId: [''],
      distributeByOrgId: [''],
      invItemFormValue: this.fb.array([
        this.initInvItem()
      ])
    })
    //add form end
  }

  get invItem() { return this.addForm.get('invItem') as FormArray }

  initInvItem() {
    return this.fb.group({
      itemId: [''],
      uom: [''],
      invCountryOfOriginId: [''],
      invBrandId: [''],
      brand: [''],
      quantity: [''],
      remarks: ['']
    });
  }



  getBenefitItems() {
    this.httpService.getWithToken(this.benefitAllUrl).subscribe(res => {
      this.benefitItems = res
    })
  }

  onBenefitItemSelected(value: number) {
    this.httpService.getWithToken(this.receivedByBenefitItemId + '/' + value).subscribe(data => {



      let div = 'receiveSelect';
      let option = '<option value="">Select</option>';

      if (data.length) {
        for (let d of data) {
          option += `<option value='${d.id}'>${d.receiveNo}</option>`;
        }
      }
      else {
        option += '<option value="">No record found</option>'
      }

      document.querySelector("#" + div).innerHTML = option;
    })
  }

  onReceiveSelected(value: number) {

    this.httpService.getWithToken(this.getSingleReceiveById + '/' + value).subscribe(data => {
      this.receivedItemList = data[0].invItemBackendValue;
    })

  }

  onSubmit() {
    console.log(this.addForm.value);
    this.httpService.postWithToken(this.distributSaveUrl, this.addForm.value).subscribe(
    
       res => {
        this.msgService.add(res.messgae);
        if (res.status == 'ok') {
          this.addForm.reset();
        }
      }
    )
  }
}
