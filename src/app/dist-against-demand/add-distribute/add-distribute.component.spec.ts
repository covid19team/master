import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddDistributeComponent } from './add-distribute.component';

describe('AddDistributeComponent', () => {
  let component: AddDistributeComponent;
  let fixture: ComponentFixture<AddDistributeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddDistributeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddDistributeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
