import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DistAgainstDemandComponent } from './dist-against-demand.component';

describe('DistAgainstDemandComponent', () => {
  let component: DistAgainstDemandComponent;
  let fixture: ComponentFixture<DistAgainstDemandComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DistAgainstDemandComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DistAgainstDemandComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
