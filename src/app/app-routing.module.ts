import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ItemgroupComponent } from './itemgroup/itemgroup.component';
import { NotfoundComponent } from './notfound/notfound.component';
import { AuthGuard } from './guard/auth.guard';
import { GuestGuard } from './guard/guest.guard';

const routes: Routes = [
  {
    path:'',
    component: ItemgroupComponent,
    canActivate:[AuthGuard]
  },
  {
    path:'login',
    loadChildren:'./login/login.module#LoginModule',
    canActivate:[GuestGuard]
  },
  {
    path:'forgot-password',
    loadChildren:'./forgotpassword/forgotpassword.module#ForgotpasswordModule',
    canActivate:[GuestGuard]
  },
  {
    path:'dashboard/item-sub-group',
    loadChildren: './item-subgroup/item-subgroup.module#ItemSubgroupModule',
    canActivate:[AuthGuard]
  },
  {
    path:'dashboard/items',
    loadChildren: './items/items.module#ItemsModule',
    canActivate:[AuthGuard]
  },
  {
    path:'dashboard/demand',
    loadChildren: './demand/demand.module#DemandModule',
    canActivate:[AuthGuard],

  },
  {
    path:'dashboard/issue-against-demand',
    loadChildren: './issue-against-demand/issue-against-demand.module#IssueAgainstDemandModule',
    canActivate:[AuthGuard]
  },
  {
    path:'dashboard/receive-against-issue',
    loadChildren: './receive-against-demand/receive-against-demand.module#ReceiveAgainstDemandModule',
    canActivate:[AuthGuard]
  },
  {
    path:'dashboard/distribute-against-demand',
    loadChildren: './dist-against-demand/dist-against-demand.module#DistAgainstDemandModule',
    canActivate:[AuthGuard]
  },
  {
    path:'dashboard/damage',
    loadChildren: './damage/damage.module#DamageModule',
    canActivate:[AuthGuard]
  },
  {
    path:'dashboard/stock',
    loadChildren:'./stock/stock.module#StockModule',
    canActivate:[AuthGuard]
  },
  {
    path:'dashboard/stock-report',
    loadChildren: './stock-report/stock-report.module#StockReportModule',
    canActivate:[AuthGuard]
  },
  {
    path:'**',
    component: NotfoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
