import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardnavComponent } from './dashboardnav/dashboardnav.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { ItemgroupComponent } from './itemgroup/itemgroup.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
//interceptor-->
import { HttpConfigInterceptor } from './interceptor/httpconfig.interceptor';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {MatProgressBarModule} from '@angular/material/progress-bar'
import {MatButtonModule} from '@angular/material/button';
import { NotfoundComponent } from './notfound/notfound.component';
import { MatDialogModule,MatFormFieldModule,MatInputModule } from '@angular/material';
import { ErrordialogService} from './services/errordialog.service'
import { ErrordialogComponent } from './errordialog/errordialog.component';
import { MessageComponent } from './message/message.component';
import { LoaderComponent } from './loader/loader.component';
import { MatProgressSpinnerModule} from '@angular/material/progress-spinner'
import { LoaderService } from './services/loader.service';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import {MatAutocompleteModule} from '@angular/material/autocomplete';


@NgModule({
  declarations: [
    AppComponent,
    DashboardnavComponent,
    SidebarComponent,
    ItemgroupComponent,
    NotfoundComponent,
    ErrordialogComponent,
    MessageComponent,
    LoaderComponent,
   
 
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatProgressBarModule,
    MatButtonModule,
    MatDialogModule,
    MatProgressSpinnerModule,
    FormsModule,
    ReactiveFormsModule,
    MatAutocompleteModule,
    MatFormFieldModule,
    MatInputModule
    
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: HttpConfigInterceptor, multi: true },//interceptor
    ErrordialogService,
    LoaderService
  ],
  entryComponents: [ErrordialogComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
