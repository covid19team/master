import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReceiveAgainstDemandComponent } from './receive-against-demand.component';
import {AddReceiveComponent} from './add-receive/add-receive.component';
import {ReceiveMainComponent} from './receive-main/receive-main.component';

const routes: Routes = [
  {
    path:'',
    component: ReceiveMainComponent,
    children: [
      {
        path:'',
        component: ReceiveAgainstDemandComponent
      },
      {
        path:'add',
        component:AddReceiveComponent
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReceiveAgainstDemandRoutingModule { }
