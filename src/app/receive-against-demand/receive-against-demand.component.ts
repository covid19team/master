import { Component, OnInit } from '@angular/core';
import { MessageService } from '../services/message.service';
import { HttpService } from '../services/http.service';
import { FormBuilder, FormArray } from '@angular/forms';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-receive-against-demand',
  templateUrl: './receive-against-demand.component.html',
  styleUrls: ['./receive-against-demand.component.css']
})
export class ReceiveAgainstDemandComponent implements OnInit {
  title = "Received Details";
  receivedList: any = [];
  singleReceiveItemList: any = [];
  allReceivedUrl = "receive/getAll";
  singleReceiveById = "receive/get"
  closeResult: string;
  benefitItemName: string;
  receiveByOrgName: string;
  requestedToOrgName: string;
  receiveNo: string;
  donateName: string;
  contractAddress: string;
  receiveDateTime: any = '';
  status: string;

  constructor(
    private httpService: HttpService,
    private modalService: NgbModal
  ) { }

  ngOnInit() {

    this.getReceivedList();
  }
  // get All Received
  getReceivedList() {

    this.httpService.getWithToken(this.allReceivedUrl).subscribe(resp => {

      this.receivedList = resp;
    })
  }

  //get single received List
  getReceiveById(id: number) {

    this.httpService.getWithToken(this.singleReceiveById + "/" + id).subscribe(res => {
      this.receiveNo = res[0].receiveNo;
      this.donateName = res[0].donateName;
      this.contractAddress = res[0].contractAddress;
      this.singleReceiveItemList = res[0].invItemBackendValue;
      this.benefitItemName = res[0].benefitItemName;
      this.receiveByOrgName = res[0].receiveByOrgName;
      this.requestedToOrgName = res[0].requestedTogName;
      this.status = res[0].status;
      this.receiveDateTime = res[0].receiveDate;
    })


  }




  //Open View Modal Pop-up
  open(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title', size: 'lg', windowClass: 'custom-class' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  
}
