import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReceiveAgainstDemandComponent } from './receive-against-demand.component';

describe('ReceiveAgainstDemandComponent', () => {
  let component: ReceiveAgainstDemandComponent;
  let fixture: ComponentFixture<ReceiveAgainstDemandComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReceiveAgainstDemandComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReceiveAgainstDemandComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
