import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ReceiveAgainstDemandRoutingModule } from './receive-against-demand-routing.module';
import { ReceiveAgainstDemandComponent } from './receive-against-demand.component';
import { AddReceiveComponent } from './add-receive/add-receive.component';
import { ReceiveMainComponent } from './receive-main/receive-main.component';
import {MatTreeModule} from '@angular/material/tree';
import {MatIconModule} from '@angular/material/icon';
import {MatFormFieldModule,MatInputModule} from '@angular/material';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [ReceiveAgainstDemandComponent, AddReceiveComponent, ReceiveMainComponent],
  imports: [
    CommonModule,
    ReceiveAgainstDemandRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    MatTreeModule,
    MatIconModule,
    MatFormFieldModule,
    MatInputModule
  ]
})
export class ReceiveAgainstDemandModule { }
