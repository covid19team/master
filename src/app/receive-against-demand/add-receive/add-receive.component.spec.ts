import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddReceiveComponent } from './add-receive.component';

describe('AddReceiveComponent', () => {
  let component: AddReceiveComponent;
  let fixture: ComponentFixture<AddReceiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddReceiveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddReceiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
