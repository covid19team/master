import { Component, OnInit, NgModule } from '@angular/core';
import { MessageService } from 'src/app/services/message.service';
import { HttpService } from 'src/app/services/http.service';
import { FormBuilder, FormArray } from '@angular/forms';
//tree view
import { NestedTreeControl } from '@angular/cdk/tree';
import { MatTreeNestedDataSource } from '@angular/material/tree';

interface FoodNode {
  id: number,
  organizationName: string;
  organizations?: FoodNode[];
}
//tree view end

@Component({
  selector: 'app-add-receive',
  templateUrl: './add-receive.component.html',
  styleUrls: ['./add-receive.component.css']
})
export class AddReceiveComponent implements OnInit {

  orgGetUrl = "organization/ministry";
  benefitAllUrl = "benefititem/get/all";
  issueNoByItemId = "report/getMyIssueListBasedOnBenefit";
  groupItemUrl = "itemgroup/get/all";
  subgroupBygroupIdUrl = "itemgroup/get/child";
  itemBySubgroupIdUrl = "itemsubgroup/get/child";
  itemDetailUrl = "item/get/child";
  getStockUrl = "stock/getStock";
  getSingleIssueById = "invIssue/get";
  receivedSaveUrl = "receive/save";

  TREE_DATA: FoodNode[] = [];//tree data
  benefitItems: any = [];
  addForm: any;
  groupItem: any = [];
  subgroupItem: any = [];
  issueedItemList: any = [];
  donateCheck: boolean = false;
  showIssueSelect: boolean = true;
  showRecOrgTree: boolean = true;
  showAddItemButton: boolean = false;
  showDonerName: boolean = false;
  showDonerAddress: boolean = false;
  showDonateItemTable:boolean = false;
  showNonDonateItemTable: boolean = true;
  //tree view
  treeControl = new NestedTreeControl<FoodNode>(node => node.organizations);
  dataSource = new MatTreeNestedDataSource<FoodNode>();
  hasChild = (_: number, node: FoodNode) => !!node.organizations && node.organizations.length > 0;//tree
  //tree view end
  constructor(
    private msgService: MessageService,
    private httpService: HttpService,
    private fb: FormBuilder
  ) { }

 
  ngOnInit() {

    //get org tree data from api
    this.httpService.oldGetWithToken(this.orgGetUrl).subscribe(
      res => {
        this.TREE_DATA = res;
        this.dataSource.data = this.TREE_DATA;
      }
    )
    //get org tree data from api end
    this.getBenefitItems();
    //this.getGroupItems();
    //add form 
    this.addForm = this.fb.group({
      donateName: '',
      contractAddress: '',
      donate: this.donateCheck,
      ownOrganization: [''],
      benefitItemId: [''],
      issueId: [''],
      totalNum: [''],
      requestedTo: [''],
      invItemFormValue: this.fb.array([
        this.initInvItem()
      ])
    })
    //add form end
  }

  get invItem() { return this.addForm.get('invItemFormValue') as FormArray }

  initInvItem() {
    return this.fb.group({
      itemId: [''],
      uom: [''],
      invCountryOfOriginId: [''],
      invBrandId: [''],
      brand: [''],
      quantity: [''],
      remarks: ['']
    });
  }



  getBenefitItems() {
    this.httpService.getWithToken(this.benefitAllUrl).subscribe(res => {
      this.benefitItems = res
    })
  }

  // benefit Item Select dropdown event

  onBenefitItemSelected(value: number) {
    this.httpService.getWithToken(this.issueNoByItemId + '/' + value).subscribe(data => {

      if (this.donateCheck == false) {

        let div = 'issueSelect';
        let option = '<option value="">Select</option>';

        if (data.length) {
          for (let d of data) {
            option += `<option value='${d.id}'>${d.issueNo}</option>`;
          }
        }
        else {
          option += '<option value="">No record found</option>'
        }

        document.querySelector("#" + div).innerHTML = option;

      }


    })
  }
  // Issue Select dropdown event

  onIssueSelected(value: number) {
    
     this.httpService.getWithToken(this.getSingleIssueById+'/'+value).subscribe(data => {
     console.log(data);
      this.issueedItemList = data.invItem;
      console.log(this.issueedItemList);
     })
    
  }

  getGroupItems() {
    this.httpService.getWithToken(this.groupItemUrl).subscribe(res => {
      this.groupItem = res;
    })
  }

  //take 2 param index and group id
  getSubgroupItems(index: number, groupId: number) {
    let div = 'subgroupId' + index;
    let markup = '<option value="">Select</option>';

    if (groupId) {
      this.httpService.getWithToken(this.subgroupBygroupIdUrl + '/' + groupId).subscribe(
        res => {
          if (res.length) {
            for (let r of res) {
              markup += `<option value='${r.id}'>${r.name}</option>`;
            }
          }
          else {
            markup += '<option value="">No record found</option>'
          }

          document.querySelector("#" + div).innerHTML = markup;
        },
      )
    }
  }

  getItems(index: number, subgroupId: number) {
    //take 2 param index and event

    let div = 'itemId' + index;
    let markup = '<option value="">Select</option>';

    if (subgroupId) {
      this.httpService.getWithToken(this.itemBySubgroupIdUrl + '/' + subgroupId).subscribe(
        res => {

          if (res.length) {
            for (let r of res) {
              markup += `<option value='${r.id}' id="${r.uom}">${r.name}</option>`;
            }
          }
          else {
            markup += '<option value="">No record found</option>'
          }

          document.querySelector("#" + div).innerHTML = markup;
        },
      )
    }

  }

  setItemFeature(i: number, uom: string, itemId: number) {

    let uomDiv = '#uomId' + i;
    let corDiv = '#cor' + i;
    let brandDiv = '#brand' + i;
    let corMarkup = `<option value=''>Select</option>`;
    let brandMarkup = `<option value=''>Select</option>`;
    document.querySelector(uomDiv).innerHTML += `<option value="${uom}" selected>${uom}</option>`;
    //get item detail
    if (itemId) {
      this.httpService.getWithToken(this.itemDetailUrl + '/' + itemId).subscribe(
        res => {

          for (let cof of res.countryOrigins) {
            corMarkup += `<option value="${cof.id}">${cof.name}</option>`;
          }
          for (let brand of res.brands) {
            brandMarkup += `<option value="${brand.id}">${brand.name}</option>`;
          }
          document.querySelector(corDiv).innerHTML = corMarkup;
          document.querySelector(brandDiv).innerHTML = brandMarkup;
        }
      )
    }

  }
  //hide show by donate type check
  onCheckboxChange(event) {

    if (event.target.checked) {
      this.donateCheck = true;
      this.showIssueSelect = !this.showIssueSelect;
      this.showRecOrgTree = false;
      this.showAddItemButton = true;
      this.showDonerName = true;
      this.showDonerAddress = true;
      this.showDonateItemTable = true;
      this.showNonDonateItemTable = false;
      this.getGroupItems();
    } else {
      this.donateCheck = false;
      this.showIssueSelect = true;
      this.showRecOrgTree = true;
      this.showAddItemButton = false;
      this.showDonerName = false;
      this.showDonerAddress = false;
      this.showDonateItemTable = false;
      this.showNonDonateItemTable = true;
    }
  }
  addInvItem() {
    this.invItem.push(this.initInvItem());
  }

  removeInvItem(i: number) {
    this.invItem.removeAt(i);
  }

  onSubmit() {

    this.httpService.postWithToken(this.receivedSaveUrl, this.addForm.value).subscribe(
      res => {
        this.msgService.add(res.messgae);
        if (res.status == 'ok') {
          this.addForm.reset();
        }
      }
    )
  }
  
}
