import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IssueAgainstDemandRoutingModule } from './issue-against-demand-routing.module';
import { IssueAgainstDemandComponent } from './issue-against-demand.component';
import { AddIssueComponent } from './add-issue/add-issue.component';
import { IssueMainComponent } from './issue-main/issue-main.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatTreeModule} from '@angular/material/tree';
import {MatIconModule} from '@angular/material/icon';
import {MatFormFieldModule,MatInputModule} from '@angular/material';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import { IssueDetailComponent } from './issue-detail/issue-detail.component';

@NgModule({
  declarations: [IssueAgainstDemandComponent, AddIssueComponent, IssueMainComponent, IssueDetailComponent],
  imports: [
    CommonModule,
    IssueAgainstDemandRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MatTreeModule,
    MatIconModule,
    MatAutocompleteModule,
    MatFormFieldModule,
    MatInputModule
  ]
})
export class IssueAgainstDemandModule { }
