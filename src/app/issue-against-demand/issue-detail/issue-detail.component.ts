import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpService } from 'src/app/services/http.service';
import { MessageService } from 'src/app/services/message.service';

@Component({
  selector: 'app-issue-detail',
  templateUrl: './issue-detail.component.html',
  styleUrls: ['./issue-detail.component.css']
})
export class IssueDetailComponent implements OnInit {

  issueDetailUrl = "invIssue/get";
  title ="Issue Detail";
  detail:any=[];
  constructor(
    private route : ActivatedRoute,
    private http : HttpService,
    private msg : MessageService
  ) { }

  ngOnInit() {
    const issueId = this.route.snapshot.paramMap.get('id');
    this.issueDetail(issueId);
  }

  issueDetail(issueId:any)
  {
    this.http.getWithToken(this.issueDetailUrl+'/'+issueId).subscribe(res=>{
      
      this.detail = res;
      this.msg.add('Issue detail fetched!');
    })
  }

}
