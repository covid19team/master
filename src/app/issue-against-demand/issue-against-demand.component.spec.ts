import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IssueAgainstDemandComponent } from './issue-against-demand.component';

describe('IssueAgainstDemandComponent', () => {
  let component: IssueAgainstDemandComponent;
  let fixture: ComponentFixture<IssueAgainstDemandComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IssueAgainstDemandComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IssueAgainstDemandComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
