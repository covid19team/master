import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IssueAgainstDemandComponent } from './issue-against-demand.component';
import {AddIssueComponent} from './add-issue/add-issue.component';
import {IssueMainComponent} from './issue-main/issue-main.component';
import { IssueDetailComponent } from './issue-detail/issue-detail.component';

const routes: Routes = [
  {
    path:'',
    component: IssueMainComponent,
    children: [
      {
        path:'',
        component: IssueAgainstDemandComponent
      },
      {
        path:'add',
        component: AddIssueComponent
      },
      {
        path:'detail/:id',
        component: IssueDetailComponent
      }   
    ]
  },
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IssueAgainstDemandRoutingModule { }
