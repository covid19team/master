import { Component, OnInit } from '@angular/core';
import { MessageService } from '../services/message.service';
import { HttpService } from '../services/http.service';
import { FormBuilder,FormArray } from '@angular/forms';

@Component({
  selector: 'app-issue-against-demand',
  templateUrl: './issue-against-demand.component.html',
  styleUrls: ['./issue-against-demand.component.css']
})
export class IssueAgainstDemandComponent implements OnInit {
  //getIssueUrl = "invIssue/get/all";
  getIssueUrl = "invIssue/get/getAllIssuedBy";
  issues:any= [];
  title ="Issue Details";
  constructor(
    private httpService: HttpService,
    private msgService: MessageService
  ) { }

  ngOnInit() {
    this.getIssue();
  }

  getIssue()
  {
    this.httpService.getWithToken(this.getIssueUrl).subscribe(res=>{
      res.length ? this.msgService.add('Issue fetched successfully!') :this.msgService.add('No record found!');
      this.issues = res;
    })
  }
}
