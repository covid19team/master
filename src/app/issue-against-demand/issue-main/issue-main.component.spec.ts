import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IssueMainComponent } from './issue-main.component';

describe('IssueMainComponent', () => {
  let component: IssueMainComponent;
  let fixture: ComponentFixture<IssueMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IssueMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IssueMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
