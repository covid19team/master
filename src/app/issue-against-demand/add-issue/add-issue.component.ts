import { Component, OnInit } from '@angular/core';
import { MessageService } from 'src/app/services/message.service';
import { HttpService } from 'src/app/services/http.service';
import { FormBuilder, FormArray,FormControl } from '@angular/forms';
import {NestedTreeControl} from '@angular/cdk/tree';
import {MatTreeNestedDataSource} from '@angular/material/tree';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';

interface FoodNode {
  id:number,
  organizationName: string;
  organizations?: FoodNode[];
}

@Component({
  selector: 'app-add-issue',
  templateUrl: './add-issue.component.html',
  styleUrls: ['./add-issue.component.css']
})
export class AddIssueComponent implements OnInit {
  orgGetUrl = "organization/ministry";
  benefitAllUrl = "benefitItem/all";
  subgroupAllUrl = "itemsubgroup/get/all";
  demandByBenfitIdUrl = "report/getMyDemandListBasedOnBenefit";
  demandDetailUrl = "invDemand/get";
  itemBySubgroupUrl = "itemsubgroup/get/child";
  issueSaveUrl = "invIssue/save";
  itemDetailUrl = "item/get/child"; 
  benefitItems: any = [];
  subgroup: any = [];
  addForm: any;
  TREE_DATA: FoodNode[] =[];

  //tree view
treeControl = new NestedTreeControl<FoodNode>(node => node.organizations);
dataSource = new MatTreeNestedDataSource<FoodNode>();
//tree view end

/* Auto Complete */
myControl = new FormControl();
demandData: any= [];
filteredOptions: Observable<string[]>;
/* end AutoComplete */


  constructor(
    private msgService: MessageService,
    private httpService: HttpService,
    private fb: FormBuilder
  ) { }
  hasChild = (_: number, node: FoodNode) => !!node.organizations && node.organizations.length > 0;//tree
  ngOnInit() {
      //get org tree data from api
      this.httpService.oldGetWithToken(this.orgGetUrl).subscribe(
        res=> {
          this.TREE_DATA = res;
          this.dataSource.data = this.TREE_DATA;
        }
      )
    this.getBenefitItems();
    this.getSubgroup();

    //add form 
    this.addForm = this.fb.group({
      
      benefitItemId:[''],
      demandList:[''],
      requestedTo:[''],
      invItem: this.fb.array([])
    })
    //add form end
  }

  get invItem(){return this.addForm.get('invItem') as FormArray}

// get benefit items for dropdown
  getBenefitItems()
  {
    this.httpService.oldGetWithToken(this.benefitAllUrl).subscribe(res=>{
      this.benefitItems = res
    })
  }
//demand data for auto complete
  getDemandNumber (benefitId:number) 
  {
    this.httpService.getWithToken(this.demandByBenfitIdUrl+'/'+benefitId).subscribe(data =>{
      this.demandData = data;
      
    })
  }
  initItem()
  {
    return this.fb.group({
      itemId:[''],
      invCountryOfOriginId:[''],
      invBrandId:[''],
      brand:[''],
      quantity:[''],
      remarks:['']
    })    
  }
  demandDetail(id:number){
    //crreating var for div
    if(!id){return ;}
    let key =0;
    this.httpService.getWithToken(this.demandDetailUrl+'/'+id).subscribe(
      res=> {
        //clear form array 
        while(this.invItem.length !==0){this.invItem.removeAt(0)}
         //custom index
         for (let item of res.invItem)
         {
        
         
           this.invItem.push(
             this.fb.group({
               itemId:[item.itemId],
               invCountryOfOriginId:[item.invCountryOfOriginId],
               invBrandId:[item.invBrandId],
               quantity:[item.quantity],
               remarks:[item.remarks]
             })
           )

          setTimeout(()=>{
            document.querySelector("#subgroupId"+key ).innerHTML += `<option value='${item.itemSubGroupId}' selected>${item.itemSubGroupName}</option>`; 

            document.querySelector("#itemDiv"+key).innerHTML = `<option value="${item.itemId}">${item.item}</option>`;
            

            document.querySelector("#originId"+key).innerHTML = `<option value='${item.invCountryOfOriginId}'>${item.invCountryOfOrigin}</option>`;
  
            document.querySelector("#brandId"+key).innerHTML = `<option value='${item.invBrandId}'>${item.invBrand}</option>`;              
            key++;
          },100);
          
         }

      }
    )
    
  }

  getSubgroup()
  {
    this.httpService.getWithToken(this.subgroupAllUrl).subscribe(res=>{
      this.subgroup = res;
      
    })    
  }

  changeItemBySubgroup(index:number,event:any)
  {
    let id = event.target.value;

    let markup;
    this.httpService.getWithToken(this.itemBySubgroupUrl+'/'+id).subscribe(
      res=> {
      
        for(let r of res)
        {
          markup += `<option value='${r.id}'>${r.name}</option>`
        }
        document.querySelector('#itemDiv'+index).innerHTML = markup;
      }
    )
  }

  onSubmit()
  {


    let arr = [];
    let demand = parseInt(this.addForm.get('demandList').value);
    arr.push(demand);
    
    this.addForm.controls['demandList'].patchValue(arr);
    console.log(this.addForm.value);
    this.httpService.postWithToken(this.issueSaveUrl,this.addForm.value).subscribe(
      res => {
        this.msgService.add(res.messgae)
      }
    )

  }
  itemDetail(event,index){
    let itemId = event.target.value;
    let origin = '<option>Select</option>';
    let brand = '<option>Select</option>';
    this.httpService.getWithToken(this.itemDetailUrl+'/'+itemId).subscribe(
      res =>{
        for(let r of res.countryOrigins){
          origin += `<option value="${r.id}">${r.name}</option>`;
        }
        for(let r of res.brands){
          brand += `<option value="${r.id}">${r.name}</option>`;
        }
        document.querySelector('#originId'+index).innerHTML = origin;
        document.querySelector('#brandId'+index).innerHTML = brand;
      }
    )
  }
}
