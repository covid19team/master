import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';
import { MessageService } from 'src/app/services/message.service';

@Component({
  selector: 'app-stock-list',
  templateUrl: './stock-list.component.html',
  styleUrls: ['./stock-list.component.css']
})
export class StockListComponent implements OnInit {
  title ="Stock";
  getAllUrl = 'stock/getByAll';

  pageNum: number =0;
  stocks:any = [];

  constructor(
    private httpService: HttpService,
    private msgService: MessageService
  ) { }

  ngOnInit() {
    this.stockList();
  }

  stockList(pageNumber=0,pageSize=10){
    this.pageNum = pageNumber;
    let post = {pageNumber:pageNumber,pageSize:pageSize};
    this.httpService.postWithToken(this.getAllUrl,post).subscribe(
      res => {
        
        this.msgService.add('Stock fetched successfully!');
        this.stocks = res;
      }
    )
  }

}
