import { Component, OnInit } from '@angular/core';
//tree view
import {NestedTreeControl} from '@angular/cdk/tree';
import {MatTreeNestedDataSource} from '@angular/material/tree';
import { HttpService } from 'src/app/services/http.service';
import { MessageService } from 'src/app/services/message.service';
import { FormBuilder } from '@angular/forms';
interface FoodNode {
  id:number,
  organizationName: string;
  organizations?: FoodNode[];
}
//tree view end

@Component({
  selector: 'app-add-stock',
  templateUrl: './add-stock.component.html',
  styleUrls: ['./add-stock.component.css']
})
export class AddStockComponent implements OnInit {
  orgGetUrl = "organization/ministry";
  itemGetAll = "item/get/all";
  itemDetailUrl = "item/get/child";
  saveUrl = "stock/save";

  TREE_DATA: FoodNode[] =[];//tree data
  items:any = [];
  addForm :any;
  origin:any = [];
  brand:any = [];
//tree view
  treeControl = new NestedTreeControl<FoodNode>(node => node.organizations);
  dataSource = new MatTreeNestedDataSource<FoodNode>();
//tree view end  
  constructor(
    private httpService: HttpService,
    private fb: FormBuilder,
    private msgService: MessageService
  ) { }

  hasChild = (_: number, node: FoodNode) => !!node.organizations && node.organizations.length > 0;//tree
  ngOnInit() {
    //get org tree data from api
    this.httpService.oldGetWithToken(this.orgGetUrl).subscribe(
      res=> {
        this.TREE_DATA = res;
        this.dataSource.data = this.TREE_DATA;
      }
    )
    //get org tree data from api end   
    //get items 
    this.httpService.getWithToken(this.itemGetAll).subscribe(
      res=> this.items = res
    )    
      this.addForm = this.fb.group({
        organizationId: [''],
        itemId: [''],
        invCountryOfOriginId:[''],
        invBrandId: [''],
        quantity: ['']
      })
  }

  itemDetail(id:number){
    this.httpService.getWithToken(this.itemDetailUrl+'/'+id).subscribe(
      res=> {
        this.origin = res.countryOrigins;
        this.brand = res.brands;
      }
    )
  }

  onSubmit(){
    this.httpService.postWithToken(this.saveUrl,this.addForm.value).subscribe(
      res=> {
        this.msgService.add(res.messgae);
        if(res.satus="ok"){this.addForm.reset()}
      }
    )
  }
}
